"""
    PyRCSwitch - Raspberry Pi libary for remote control outlet switches
    Python port of (part of) the rc-switch C++ library by Suat Özgür.

    Copyright (c) 2023 Patrick Günther

    Original library's author's notice:
     |  Copyright (c) 2011 Suat Özgür.  All right reserved.
     | 
     |  Contributors:
     |    - Andre Koehler / info(at)tomate-online(dot)de
     |    - Gordeev Andrey Vladimirovich / gordeev(at)openpyro(dot)com
     |    - Skineffect / http://forum.ardumote.com/viewtopic.php?f=2&t=46
     |    - Dominik Fischer / dom_fischer(at)web(dot)de
     |    - Frank Oltmanns / <first name>.<last name>(at)gmail(dot)com
     |    - Andreas Steinel / A.<lastname>(at)gmail(dot)com
     |    - Max Horn / max(at)quendi(dot)de
     |    - Robert ter Vehn / <first name>.<last name>(at)gmail(dot)com
     |    - Johann Richard / <first name>.<last name>(at)gmail(dot)com
     |    - Vlad Gheorghe / <first name>.<last name>(at)gmail(dot)com https://github.com/vgheo
     |    - Matias Cuenca-Acuna 
     | 
     |  Project home: https://github.com/sui77/rc-switch/



    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from gpiozero import DigitalOutputDevice
from time import sleep

class HighLow:
    def __init__(self, high, low):
        self.high = high
        self.low = low

class Protocol:
    def __init__(self, pulse_length, sync_factor, zero, one, inverted):
        self.pulse_length = pulse_length
        self.sync_factor = sync_factor
        self.zero = zero
        self.one = one
        self.inverted = inverted

protocols = [
    Protocol(350, HighLow(  1,  31), HighLow( 1,  3), HighLow( 3,  1), False),    # protocol 0
    Protocol(650, HighLow(  1,  10), HighLow( 1,  2), HighLow( 2,  1), False),    # protocol 1
    Protocol(100, HighLow( 30,  71), HighLow( 4, 11), HighLow( 9,  6), False),    # protocol 2
    Protocol(380, HighLow(  1,   6), HighLow( 1,  3), HighLow( 3,  1), False),    # protocol 3
    Protocol(500, HighLow(  6,  14), HighLow( 1,  2), HighLow( 2,  1), False),    # protocol 4
    Protocol(450, HighLow( 23,   1), HighLow( 1,  2), HighLow( 2,  1), True),     # protocol 5 (HT6P20B)
    Protocol(150, HighLow(  2,  62), HighLow( 1,  6), HighLow( 6,  1), False),    # protocol 6 (HS2303-PT, i. e. used in AUKEY Remote)
    Protocol(200, HighLow(  3, 130), HighLow( 7, 16), HighLow( 3, 16), False),    # protocol 7 Conrad RS-200 RX
    Protocol(200, HighLow(130,   7), HighLow(16,  7), HighLow(16,  3), True),     # protocol 8 Conrad RS-200 TX
    Protocol(365, HighLow( 18,   1), HighLow( 3,  1), HighLow( 1,  3), True),     # protocol 9 (1ByOne Doorbell)
    Protocol(270, HighLow( 36,   1), HighLow( 1,  2), HighLow( 2,  1), True),     # protocol 10 (HT12E)
    Protocol(320, HighLow( 36,   1), HighLow( 1,  2), HighLow( 2,  1), True)      # protocol 11 (SM5212)
]

class RCSwitch:
    def __init__(self, pin, protocol=0, repeat_transmit=10):
        self.pin = DigitalOutputDevice(pin)
        self.repeat_transmit = repeat_transmit 
        self.protocol = protocol
        self._value = None

    def __del__(self):
        self.close()

    def close(self):
        self.pin.close()

    #@property
    #def value(self):
    #    return value

    #@value.setter
    #def value(self, v):
    #    self._value = v
    #    if v:
    #        self.switch_on(

    @property
    def protocol(self):
        return self._protocol

    @protocol.setter
    def protocol(self, protocol):
        """
        Sets the protocol to send, from a list of predefined protocols or directly
        """
        if not type(protocol) is Protocol:
            if protocol < 0 or protocol >= len(protocols):
                raise ValueError("Invalid protocol")
            self._protocol = protocols[protocol]
        else:
            self._protocol = protocol

    def send_tri_state(self, code_word : str):
        """
        @param code_word: a tristate code word consisting of the letter 0, 1, F
        """
        #turn the tristate code word into the corresponding bit pattern, then send it
        code = 0
        
        for p in code_word:
            code <<= 2
            if p == "F":
                code |= 1
            elif p == "1":
                code |= 3
        

        self.send(code, 2 * len(code_word));

    def send_code_word(self, code_word : str):
        """
        @param code_word: a binary code word consisting of the letter 0, 1
        """
        # turn the tristate code word into the corresponding bit pattern, then send it
        code = 0
        for p in code_word:
            code <<= 1
            if p != '0':
                code |= 1

        self.send(code, len(code_word))

    def send(self, code : int, length : int):
        """
        Transmit the first 'length' bits of the integer 'code'. The
        bits are sent from MSB to LSB, i.e., first the bit at position length-1,
        then the bit at position length-2, and so on, till finally the bit at position 0.
        """

        for _ in range(self.repeat_transmit):
            for i in range(length - 1, -1, -1):
                if code & (1 << i):
                    self.transmit(self.protocol.one)
                else:
                    self.transmit(self.protocol.zero)

            self.transmit(self.protocol.sync_factor)

        # Disable transmit after sending (i.e., for inverted protocols)
        self.pin.off()

    def transmit(self, pulses : HighLow):
        """
        Transmit a single high-low pulse.
        """
        first_logic_level  = 0 if self.protocol.inverted else 1
        second_logic_level = 1 if self.protocol.inverted else 0

        self.pin.value = first_logic_level
        sleep(self.protocol.pulse_length * pulses.high / 1000000)
        self.pin.value = second_logic_level
        sleep(self.protocol.pulse_length * pulses.low  / 1000000)

class RCSwitchA(RCSwitch):
    """
   Switch a remote switch off (Type A with 10 pole DIP switches)
    
    @param group        Code of the switch group (refers to DIP 
        switches 1..5 where "1" = on and "0" = off, if all DIP
        switches are on it's "11111")
    @param device       Code of the switch device (refers to DIP
        switches 6..10 (A..E) where "1" = on and "0" = off, if
        all DIP switches are on it's "11111")
    """

    def switch_on(self, group : str, device : str):
        self.send_tri_state(self.get_code_word(group, device, True))


    def switch_off(self, group : str, device : str):
        self.send_tri_state(self.get_code_word(group, device, False))


    def get_code_word(self, group : str, device : str, status : bool):
        """
        Returns a char[13], representing the code word to be send.
        sGroup and sDevice are strings
        """
        word = ""

        for c in group + device:
            word += 'F' if c == '0' else '0'

        word += '0F' if status else 'F0'

        return word

class RCSwitchB(RCSwitch):
    """
    Switch a remote switch on (Type B with two rotary/sliding switches)
    
    @param nAddressCode  Number of the switch group (1..4)
    @param nChannelCode  Number of the switch itself (1..4)
    """

    def switch_on(self, address_code : int, channel_code : int):
        self.send_tri_state(self.get_code_word(address_code, channel_code, True))

    def switch_off(self, address_code : int, channel_code : int):
        self.send_tri_state(self.get_code_word(nAddressCode, channel_code, False))

    def get_code_word(self, address_code : int, channel_code : int, status : bool):
        """
        Encoding for type B switches with two rotary/sliding switches.

        The code word is a tristate word and with following bit pattern:

        +-----------------------------+-----------------------------+----------+------------+
        | 4 bits address              | 4 bits address              | 3 bits   | 1 bit      |
        | switch group                | switch number               | not used | on / off   |
        | 1=0FFF 2=F0FF 3=FF0F 4=FFF0 | 1=0FFF 2=F0FF 3=FF0F 4=FFF0 | FFF      | on=F off=0 |
        +-----------------------------+-----------------------------+----------+------------+

        @param nAddressCode  Number of the switch group (1..4)
        @param nChannelCode  Number of the switch itself (1..4)
        @param bStatus       Whether to switch on (true) or off (false)

        @return char[13], representing a tristate code word of length 12
        """
        word = 12 * "F"

        if address_code < 1 or address_code > 4 or channel_code < 1 or channel_code > 4:
            return 0

        word[address_code - 1] = "0"
        word[channel_code - 1 + 4] = "0"

        if not bStatus:
            word[11] = "0"

        return word


class RCSwitchC(RCSwitch):
    """
    Switch a remote switch on (Type C Intertechno)

    @param sFamily  Familycode (a..f)
    @param nGroup   Number of group (1..4)
    @param nDevice  Number of device (1..4)
    """

    def switch_on(self, family : str, group : int, device : int):
        self.send_tri_state(self.get_code_word(family, group, device, True))

    def switch_off(self, family : str, group : int, device : int):
        self.send_tri_state(self.get_code_word(family, group, device, False))

    def get_code_word(self, family : str, group : int, device : int, status : bool):
        """
        Like getCodeWord (Type C = Intertechno)
        """
        family = ord(family) - ord('a')

        if (family < 0 or family > 15 or group < 1 or group > 4 or device < 1 or device > 4):
            return 0

        # encode the family into four bits
        word = f"{family:04b}"[::-1]
        word += f"{device - 1:02b}"[::-1]
        word += f"{group - 1:02b}"[::-1]
        word += "011"
        word += "1" if status else "0"

        return word.replace("1", "F")

class RCSwitchD(RCSwitch):
    """
    Switch a remote switch (Type D REV)

    @param sGroup        Code of the switch group (A,B,C,D)
    @param nDevice       Number of the switch itself (1..3)
    """

    def switch_on(self, group : str, device : int):
        self.send_tri_state(self.get_code_word(group, device, True))

    def switch_off(self, group : str, device : int):
        self.send_tri_state(self.get_code_word(group, device, False))

    def get_code_word(self, group : str, device : int, status : bool):
        """
        Encoding for the REV Switch Type

        The code word is a tristate word and with following bit pattern:

        +-----------------------------+-------------------+----------+--------------+
        | 4 bits address              | 3 bits address    | 3 bits   | 2 bits       |
        | switch group                | device number     | not used | on / off     |
        | A=1FFF B=F1FF C=FF1F D=FFF1 | 1=0FF 2=F0F 3=FF0 | 000      | on=10 off=01 |
        +-----------------------------+-------------------+----------+--------------+

        Source: http://www.the-intruder.net/funksteckdosen-von-rev-uber-arduino-ansteuern/

        @param sGroup        Name of the switch group (A..D, resp. a..d) 
        @param nDevice       Number of the switch itself (1..3)
        @param bStatus       Whether to switch on (true) or off (false)

        @return char[13], representing a tristate code word of length 12
        """

        # sGroup must be one of the letters in "abcdABCD"
        ngroup = ord(group.lower()) - ord('a')
        if ( ngroup < 0 or ngroup > 3 or device < 1 or device > 3):
            return 0

        word = 7 * "F"
        word[ngroup] = "1"
        word[device - 1 + 4] = "1"

        word += "000"
        word += "10" if status else "01"

        return word

