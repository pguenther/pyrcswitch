# PyRCSwitch

_Switch 433MHz radio controlled wall plugs using a Raspberry Pi_

This library is a python port of (parts of) [the rc-switch C++ library](https://github.com/sui77/rc-switch) whose authors did the heavy lifting. 
This port intends to make it easy-to-use on a Raspberry Pi setup without the need to compile C++ code and use ([apparently deprecated](https://raspberrypi.stackexchange.com/a/133252)) wiringpi.
It currently only implements the transmission part of the original C++ library. Feel free to contribute the ported recieving part.
For further information head to the [original repository](https://github.com/sui77/rc-switch).
