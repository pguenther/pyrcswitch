from rcswitch import *
import time

# this is for one type of switches, namely the one having a 'system code'
# encoded with a 5-pin DIP switch, and a wall plug number A-D

# create an RCSwitch object
# 'A' designates the encoding, see comments in rcswitch.py for details
rcs = RCSwitchA(23, protocol=6)

# switch plug 'C' of system '01010'
rcs.switch_off("01010", "00100")
time.sleep(0.5)
rcs.switch_on("01010", "00100")
